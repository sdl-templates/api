﻿using Microsoft.Extensions.Options;
using Template.API.Options;
using Template.API.Utils;

namespace Template.API.DataBridges.V1;

public class DateDataBridge(
        IOptions<DatabaseOptions> databaseOptions
    ) : PostgresDataBridgeBase(databaseOptions.Value.TemplateDB)
{
    public async Task<string> GetDateAsync()
    {
        return await Task.FromResult(DateTime.Now.ToString("yyyy-MM-dd"));
    }
}