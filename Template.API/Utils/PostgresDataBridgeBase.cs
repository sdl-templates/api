﻿using System.Data;
using Dapper;
using Npgsql;

namespace Template.API.Utils;

public class PostgresDataBridgeBase(string connectionString) : IDapperWrapper
{
    private readonly IDbConnection _db = new NpgsqlConnection(connectionString);
    
    public async Task<T?> ProcedureSingleOrDefaultAsync<T>(string procedureName, object parameters)
    {
        return await _db.QuerySingleOrDefaultAsync<T>(
            $"SELECT * FROM {procedureName}",
            parameters,
            commandType: CommandType.Text
        );
    }

    public async Task<T> ProcedureSingleAsync<T>(string procedureName, object parameters)
    {
        return await _db.QuerySingleAsync<T>(
            $"SELECT * FROM {procedureName}",
            parameters,
            commandType: CommandType.Text
        );
    }
    
    public async Task<List<T>> ProcedureAsync<T>(string procedureName, object parameters)
    {
        return (await _db.QueryAsync<T>(
            $"SELECT * FROM {procedureName}",
            parameters,
            commandType: CommandType.Text
        )).ToList();
    }

    public async Task ProcedureExecuteAsync(string procedureName, object parameters)
    {
        await _db.ExecuteAsync(
            $"SELECT * FROM {procedureName}",
            parameters,
            commandType: CommandType.Text
        );
    }
}