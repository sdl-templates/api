﻿namespace Template.API.Utils;

public interface IDapperWrapper
{
    public Task<T?> ProcedureSingleOrDefaultAsync<T>(string procedureName, object parameters);
    public Task<T> ProcedureSingleAsync<T>(string procedureName, object parameters);
    public Task<List<T>> ProcedureAsync<T>(string procedureName, object parameters);
    public Task ProcedureExecuteAsync(string procedureName, object parameters);
}