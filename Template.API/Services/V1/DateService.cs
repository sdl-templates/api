﻿using Template.API.DataBridges.V1;

namespace Template.API.Services.V1;

public class DateService(
        DateDataBridge dateDataBridge
    )
{
    public async Task<string> GetDateAsync()
    {
        return await dateDataBridge.GetDateAsync();
    }
}