﻿using System.ComponentModel.DataAnnotations;

namespace Template.API.Options;

public class BaseOptions
{
    [Required] public string Version { get; set; } = string.Empty;
    [Required] public string CorsPolicy { get; set; } = string.Empty;
    [Required] public string[] AllowedOrigins { get; set; } = Array.Empty<string>();
}