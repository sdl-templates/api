﻿using System.ComponentModel.DataAnnotations;

namespace Template.API.Options;

public class DatabaseOptions
{
    public static string SectionName { get; } = "ConnectionStrings";
    [Required] public string TemplateDB { get; set; } = string.Empty;
}