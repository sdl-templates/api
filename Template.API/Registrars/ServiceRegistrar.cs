﻿using Template.API.Services.V1;

namespace Template.API.Registrars;

public static class ServiceRegistrar
{
    public static void Register(IServiceCollection services)
    {
        // Register services here
        services.AddScoped<DateService>();
    }
}