﻿using Template.API.Options;

namespace Template.API.Registrars;

public static class OptionRegistrar
{
    public static void AddOptions(WebApplicationBuilder builder)
    {
        // Register options here
        builder.Services
            .AddOptions<BaseOptions>()
            .Bind(builder.Configuration)
            .ValidateDataAnnotations()
            .ValidateOnStart();
        
        builder.Services
            .AddOptions<DatabaseOptions>()
            .Bind(builder.Configuration.GetSection(DatabaseOptions.SectionName))
            .ValidateDataAnnotations()
            .ValidateOnStart();
    }
}