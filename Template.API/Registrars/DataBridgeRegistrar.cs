﻿using Template.API.DataBridges.V1;

namespace Template.API.Registrars;

public static class DataBridgeRegistrar
{
    public static void Register(IServiceCollection services)
    {
        // Register data bridge classes here
        services.AddScoped<DateDataBridge>();
    }
}