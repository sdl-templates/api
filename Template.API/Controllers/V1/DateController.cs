﻿using Asp.Versioning;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Template.API.Services.V1;

namespace Template.API.Controllers.V1;

[ApiController]
[ApiVersion(1.0)]
[Route("/v{version:apiVersion}/[controller]")]
public class DateController(
        DateService dateService
    ) : ControllerBase
{
    [HttpGet, Authorize]
    [ProducesResponseType<string>(StatusCodes.Status200OK)]
    public async Task<IActionResult> GetDateAsync()
    {
        return Ok(await dateService.GetDateAsync());
    }
}