﻿using Asp.Versioning;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Template.API.Options;

namespace Template.API.Controllers.V1;

[ApiController]
[ApiVersion(1.0)]
[Route("/v{version:apiVersion}/[controller]")]
public class HealthController(
        IOptions<BaseOptions> baseOptions
    ) : ControllerBase
{
    [HttpGet("version"), Authorize]
    [ProducesResponseType<string>(StatusCodes.Status200OK)]
    public IActionResult GetVersion()
    {
        return Ok(baseOptions.Value.Version);
    }

    [HttpGet("status")]
    [AllowAnonymous]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public IActionResult GetStatus()
    {
        return Ok();
    }
}