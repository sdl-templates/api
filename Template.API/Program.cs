using System.Text.Json.Serialization;
using Mapster;
using Template.API.Configs;
using Template.API.Filters;
using Template.API.Options;
using Template.API.Registrars;

var builder = WebApplication.CreateBuilder(args);

// Add options to DI container (https://learn.microsoft.com/en-us/aspnet/core/fundamentals/configuration/options?view=aspnetcore-8.0)
OptionRegistrar.AddOptions(builder);

// Bind the app settings to the BaseOptions class for use below
var baseOptions = new BaseOptions();
builder.Configuration.Bind(baseOptions);

// Add controllers to the DI container
builder.Services
    .AddControllers()
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
    });

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddHttpContextAccessor();
builder.Services.AddCors(options =>
{
    options.AddPolicy(baseOptions.CorsPolicy,
        policy =>
        {
            policy
                .WithOrigins(baseOptions.AllowedOrigins)
                .AllowAnyHeader()
                .AllowAnyMethod();
        });
});

builder.Services.AddSwaggerGen(options =>
{
    options.OperationFilter<SwaggerDefaultValueFilter>();
    options.UseInlineDefinitionsForEnums();
});

builder.Services.AddMapster();
MapsterConfig.Configure();

VersioningConfig.AddVersioning(builder.Services);

// Register classes with the DI container
builder.Services.AddSingleton<HttpClient>();

DataBridgeRegistrar.Register(builder.Services);
ServiceRegistrar.Register(builder.Services);

// Build the DI container
var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI(options =>
{
    VersioningConfig.AddSwaggerUIVersioning(app, options);
});

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();
app.UseCors(baseOptions.CorsPolicy);

app.Run();
