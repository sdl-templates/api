﻿using Mapster;
using Template.API.Unit.Tests.V1.Date;

namespace Template.API.Configs;

public class MapsterConfig
{
    public static void Configure()
    {
        TypeAdapterConfig<DateDto, DateModel>.NewConfig()
            .Map(dest => dest.Date, src => src.date);
    }
}