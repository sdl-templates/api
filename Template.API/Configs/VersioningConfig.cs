﻿using Asp.Versioning;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.SwaggerUI;
using Template.API.Swagger;

namespace Template.API.Configs;

public static class VersioningConfig
{
    public static void AddVersioning(IServiceCollection services)
    {
        services
            .AddApiVersioning(options =>
            {
                options.DefaultApiVersion = new ApiVersion(1.0);
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.ReportApiVersions = true;
            })
            .AddApiExplorer(options =>
            {
                options.GroupNameFormat = "'v'VVV";
                options.SubstituteApiVersionInUrl = true;
            });
        services.Configure<RouteOptions>(options => { options.LowercaseUrls = true; });
        services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
    }

    public static void AddSwaggerUIVersioning(WebApplication app, SwaggerUIOptions options)
    {
        foreach (var description in app.DescribeApiVersions().Reverse())
        {
            var url = $"/swagger/{description.GroupName.ToLower()}/swagger.json";
            var name = description.GroupName.ToUpperInvariant();
            options.SwaggerEndpoint(url, name);
        }
    }
}