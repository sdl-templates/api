﻿using System.ComponentModel.DataAnnotations;

namespace Template.API.Unit.Tests.V1.Date;

public class DateModel
{
    [Required] public string Date { get; set; }
}